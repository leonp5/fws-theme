/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(self, () => {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./index.scss":
/*!********************!*\
  !*** ./index.scss ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack:///./index.scss?");

/***/ }),

/***/ "./index.ts":
/*!******************!*\
  !*** ./index.ts ***!
  \******************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nconst App_1 = __importDefault(__webpack_require__(/*! ./js/App */ \"./js/App.ts\"));\nnew App_1.default().run();\n\n\n//# sourceURL=webpack:///./index.ts?");

/***/ }),

/***/ "./js/App.ts":
/*!*******************!*\
  !*** ./js/App.ts ***!
  \*******************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nconst Initializer_1 = __importDefault(__webpack_require__(/*! ./components/Initializer/Initializer */ \"./js/components/Initializer/Initializer.ts\"));\nclass App {\n    run() {\n        globalThis.appShortCut = 'fws';\n        const initializer = new Initializer_1.default();\n        initializer.init();\n    }\n}\nexports[\"default\"] = App;\n\n\n//# sourceURL=webpack:///./js/App.ts?");

/***/ }),

/***/ "./js/components/BackToTop.ts":
/*!************************************!*\
  !*** ./js/components/BackToTop.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nclass BackToTop {\n    init(btnElement) {\n        btnElement.addEventListener('click', () => {\n            this.scrollToTop();\n        });\n        window.addEventListener('scroll', () => {\n            if (document.body.scrollTop > 270 ||\n                document.documentElement.scrollTop > 270) {\n                btnElement.classList.remove('fws-hidden');\n            }\n            else {\n                btnElement.classList.add('fws-hidden');\n            }\n        });\n    }\n    scrollToTop() {\n        window.scroll({\n            top: 0,\n            left: 0,\n            behavior: 'smooth',\n        });\n    }\n}\nexports[\"default\"] = BackToTop;\n\n\n//# sourceURL=webpack:///./js/components/BackToTop.ts?");

/***/ }),

/***/ "./js/components/Initializer/Initializer.ts":
/*!**************************************************!*\
  !*** ./js/components/Initializer/Initializer.ts ***!
  \**************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {\n    if (k2 === undefined) k2 = k;\n    var desc = Object.getOwnPropertyDescriptor(m, k);\n    if (!desc || (\"get\" in desc ? !m.__esModule : desc.writable || desc.configurable)) {\n      desc = { enumerable: true, get: function() { return m[k]; } };\n    }\n    Object.defineProperty(o, k2, desc);\n}) : (function(o, m, k, k2) {\n    if (k2 === undefined) k2 = k;\n    o[k2] = m[k];\n}));\nvar __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {\n    Object.defineProperty(o, \"default\", { enumerable: true, value: v });\n}) : function(o, v) {\n    o[\"default\"] = v;\n});\nvar __importStar = (this && this.__importStar) || function (mod) {\n    if (mod && mod.__esModule) return mod;\n    var result = {};\n    if (mod != null) for (var k in mod) if (k !== \"default\" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);\n    __setModuleDefault(result, mod);\n    return result;\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nconst classes = __importStar(__webpack_require__(/*! ./classes */ \"./js/components/Initializer/classes.ts\"));\nclass Initializer {\n    elementsWithAttributes;\n    init() {\n        setTimeout(() => {\n            this.elementsWithAttributes = Array.from(document.querySelectorAll(`[data-${globalThis.appShortCut}-class]`));\n            if (this.elementsWithAttributes.length > 0) {\n                try {\n                    this.elementsWithAttributes.forEach((htmlElement) => {\n                        const className = htmlElement.getAttribute(`data-${globalThis.appShortCut}-class`);\n                        if (className !== null) {\n                            const classToExecute = this.factory(className);\n                            classToExecute.init(htmlElement);\n                        }\n                    });\n                }\n                catch (e) {\n                    console.error(e);\n                }\n            }\n        }, 100);\n    }\n    factory(className) {\n        return new classes[className]();\n    }\n}\nexports[\"default\"] = Initializer;\n\n\n//# sourceURL=webpack:///./js/components/Initializer/Initializer.ts?");

/***/ }),

/***/ "./js/components/Initializer/classes.ts":
/*!**********************************************!*\
  !*** ./js/components/Initializer/classes.ts ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\nvar __importDefault = (this && this.__importDefault) || function (mod) {\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\n};\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nexports.ToggleSubmenu = exports.ToggleMobileMenu = exports.BackToTop = void 0;\nvar BackToTop_1 = __webpack_require__(/*! ../BackToTop */ \"./js/components/BackToTop.ts\");\nObject.defineProperty(exports, \"BackToTop\", ({ enumerable: true, get: function () { return __importDefault(BackToTop_1).default; } }));\nvar ToggleMobileMenu_1 = __webpack_require__(/*! ../ToggleMobileMenu */ \"./js/components/ToggleMobileMenu.ts\");\nObject.defineProperty(exports, \"ToggleMobileMenu\", ({ enumerable: true, get: function () { return __importDefault(ToggleMobileMenu_1).default; } }));\nvar ToggleSubmenu_1 = __webpack_require__(/*! ../ToggleSubmenu */ \"./js/components/ToggleSubmenu.ts\");\nObject.defineProperty(exports, \"ToggleSubmenu\", ({ enumerable: true, get: function () { return __importDefault(ToggleSubmenu_1).default; } }));\n\n\n//# sourceURL=webpack:///./js/components/Initializer/classes.ts?");

/***/ }),

/***/ "./js/components/ToggleMobileMenu.ts":
/*!*******************************************!*\
  !*** ./js/components/ToggleMobileMenu.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nclass ToggleMobileMenu {\n    mobileHeaderElement;\n    buttonEl;\n    hamburgerTransAnimationWrapper;\n    hamburgerBtnFirstRow;\n    hamburgerBtnSecondRow;\n    hamburgerBtnThirdRow;\n    mobileMenuEl;\n    mobileMenuIsOpen;\n    constructor() {\n        this.mobileMenuIsOpen = false;\n    }\n    init(mobileHeaderElement) {\n        this.mobileHeaderElement = mobileHeaderElement;\n        this.buttonEl = mobileHeaderElement.querySelector('button');\n        this.mobileMenuEl = document.getElementById(`${globalThis.appShortCut}-mobile-menu`);\n        this.hamburgerTransAnimationWrapper = document.getElementById(`${globalThis.appShortCut}-hamburger-trans-animation-wrapper`);\n        if (this.buttonEl !== undefined && this.mobileMenuEl !== undefined) {\n            this.hamburgerBtnFirstRow = this.buttonEl.querySelectorAll('div')[0];\n            this.hamburgerBtnSecondRow =\n                this.buttonEl.querySelectorAll('div')[1];\n            this.hamburgerBtnThirdRow = this.buttonEl.querySelectorAll('div')[2];\n            this.buttonEl.addEventListener('click', () => {\n                this.toggleMenu();\n            });\n        }\n    }\n    toggleMenu() {\n        this.handleButtonClassListToggles();\n        if (this.mobileMenuIsOpen === false) {\n            this.setMobileMenuShowStyles();\n        }\n        else {\n            this.removeMobileMenuShowStyles();\n        }\n        this.mobileMenuIsOpen = !this.mobileMenuIsOpen;\n    }\n    handleButtonClassListToggles() {\n        const firstAnimationWrapperChildEl = this.hamburgerTransAnimationWrapper.querySelectorAll('div')[0];\n        const secondAnimationWrapperChildEl = this.hamburgerTransAnimationWrapper.querySelectorAll('div')[1];\n        this.hamburgerBtnFirstRow.classList.toggle('fws-translate-x-10');\n        this.hamburgerBtnSecondRow.classList.toggle('fws-translate-x-10');\n        this.hamburgerBtnThirdRow.classList.toggle('fws-translate-x-10');\n        this.hamburgerTransAnimationWrapper.classList.toggle('fws-w-0');\n        this.hamburgerTransAnimationWrapper.classList.toggle('fws-w-12');\n        this.hamburgerTransAnimationWrapper.classList.toggle('fws-translate-x-0');\n        this.hamburgerTransAnimationWrapper.classList.toggle('-fws-translate-x-10');\n        firstAnimationWrapperChildEl.classList.toggle('fws-rotate-0');\n        firstAnimationWrapperChildEl.classList.toggle('fws-rotate-45');\n        secondAnimationWrapperChildEl.classList.toggle('-fws-rotate-0');\n        secondAnimationWrapperChildEl.classList.toggle('-fws-rotate-45');\n        document.querySelector('body')?.classList.toggle('fws-overflow-hidden');\n    }\n    removeMobileMenuShowStyles() {\n        this.mobileMenuEl.classList.add('fws-opacity-0');\n        this.mobileMenuEl.classList.remove('fws-opacity-100');\n        setTimeout(() => {\n            this.mobileMenuEl.classList.add('fws-hidden');\n            this.mobileMenuEl.classList.remove('fws-flex');\n            this.mobileMenuEl.removeAttribute('style');\n        }, 100);\n    }\n    setMobileMenuShowStyles() {\n        const height = window.innerHeight - this.mobileHeaderElement.offsetHeight;\n        this.mobileMenuEl.style.height = height + 'px';\n        this.mobileMenuEl.style.top =\n            this.mobileHeaderElement.offsetHeight + 'px';\n        this.mobileMenuEl.classList.remove('fws-hidden');\n        this.mobileMenuEl.classList.add('fws-flex');\n        setTimeout(() => {\n            this.mobileMenuEl.classList.remove('fws-opacity-0');\n            setTimeout(() => {\n                this.mobileMenuEl.classList.add('fws-opacity-100');\n            }, 50);\n        }, 50);\n    }\n}\nexports[\"default\"] = ToggleMobileMenu;\n\n\n//# sourceURL=webpack:///./js/components/ToggleMobileMenu.ts?");

/***/ }),

/***/ "./js/components/ToggleSubmenu.ts":
/*!****************************************!*\
  !*** ./js/components/ToggleSubmenu.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nclass ToggleSubmenu {\n    init(toggleBtnElement) {\n        toggleBtnElement.addEventListener('click', () => {\n            const identifier = toggleBtnElement.getAttribute(`data-${globalThis.appShortCut}-submenu-toggle`);\n            this.#toggleSubMenuState(toggleBtnElement);\n            this.#toggleSubMenuArrowStyles(toggleBtnElement);\n            this.#toggleSubMenuStyles(identifier);\n        });\n    }\n    #toggleSubMenuStyles(identifier) {\n        if (identifier !== null) {\n            const subMenu = document.querySelector(`[data-${globalThis.appShortCut}-submenu=\"${identifier}\"]`);\n            console.log(subMenu);\n            if (subMenu !== null) {\n                if (subMenu.classList.contains('fws-opacity-0') === true) {\n                    this.#setSubMenuShowStyles(subMenu);\n                    setTimeout(() => {\n                        subMenu.classList.remove('fws-opacity-0');\n                    }, 75);\n                }\n                if (subMenu.classList.contains('fws-opacity-0') === false) {\n                    subMenu.classList.add('fws-opacity-0');\n                    setTimeout(() => {\n                        this.#removeSubMenuShowStyles(subMenu);\n                    }, 75);\n                }\n            }\n        }\n    }\n    #removeSubMenuShowStyles(submenu) {\n        submenu.classList.remove('fws-h-auto');\n        submenu.classList.add('fws-max-h-0');\n        submenu.classList.remove('fws-p-5');\n    }\n    #setSubMenuShowStyles(submenu) {\n        submenu.classList.add('fws-h-auto');\n        submenu.classList.remove('fws-max-h-0');\n        submenu.classList.add('fws-p-5');\n    }\n    #toggleSubMenuState(toggleBtnElement) {\n        let state = toggleBtnElement.getAttribute(`data-${globalThis.appShortCut}-submenu-open`);\n        if (state === 'false') {\n            state = 'true';\n        }\n        else {\n            state = 'false';\n        }\n        toggleBtnElement.setAttribute(`data-${globalThis.appShortCut}-submenu-open`, state);\n    }\n    #toggleSubMenuArrowStyles(toggleBtnElement) {\n        const rotateArrow = toggleBtnElement.getAttribute(`data-${globalThis.appShortCut}-rotate`);\n        console.log(rotateArrow);\n        const arrowElements = Array.from(toggleBtnElement.querySelectorAll('div'));\n        if (rotateArrow === 'false') {\n            arrowElements[1].classList.toggle('fws-rotate-[42deg]');\n            arrowElements[1].classList.toggle('-fws-rotate-[42deg]');\n            arrowElements[1].classList.toggle('fws-origin-left');\n            arrowElements[1].classList.toggle('fws-origin-right');\n            arrowElements[2].classList.toggle('fws-rotate-[42deg]');\n            arrowElements[2].classList.toggle('-fws-rotate-[42deg]');\n            arrowElements[2].classList.toggle('fws-origin-left');\n            arrowElements[2].classList.toggle('fws-origin-right');\n        }\n        else {\n            arrowElements[0].classList.toggle('fws-rotate-180');\n            arrowElements[0].classList.toggle('fws-rotate-[270deg]');\n        }\n    }\n}\nexports[\"default\"] = ToggleSubmenu;\n\n\n//# sourceURL=webpack:///./js/components/ToggleSubmenu.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	__webpack_require__("./index.ts");
/******/ 	var __webpack_exports__ = __webpack_require__("./index.scss");
/******/ 	__webpack_exports__ = __webpack_exports__["default"];
/******/ 	
/******/ 	return __webpack_exports__;
/******/ })()
;
});