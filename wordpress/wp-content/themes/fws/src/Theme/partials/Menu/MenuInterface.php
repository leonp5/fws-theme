<?php

namespace Leonp5\fws\Theme\partials\Menu;

interface MenuInterface
{
    public function getMenuItemsByKey(string $key): array;

    public function getMenuItemsByMenuId(int $menuId): array;

    public function getFirstOrderMenuItemsByKey(string $key): array;

    public function findSubemenuItems(int $menuItemId, ?string $menuKey): array;

    public function findMenuLocationKeyByMenuId(int $menuId): ?string;

    public function findMenuItemByObjectId(int $objectId): ?object;

    public function findMenuItemByMenuItemId(int $menuItemId): ?object;

    public function findMenuLocationKeyByMenuItemId(int $menuItemId): ?string;

    public function hasSubmenu(int $menuItemId, string $menuKey): bool;

    public function getCurrenPageBgColor(): string;

    public function getQueriedObjectId(): int;

    public function hasParent(int $menuItemId): bool;

    public function determineMenuItemHierarchy(int $menuItemId): int;

    /**
     * @return array<string, int>
     */
    public function getAllLocations(): array;
}
