<?php

declare(strict_types=1);

/**
 * Template body
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Theme\partials\Menu\Menu;
use Leonp5\fws\Theme\partials\PostPagination\PostPagination;

$container = (new Container())->getInstance();
$menu = $container->get(Menu::class);

?>

<body class="fws-bg-white">
    <div class="md:fws-m-5 fws-mx-auto fws-flex fws-min-h-[95vh] fws-justify-center">
        <div class="fws-grid fws-grid-cols-9 fws-grid-rows-[minmax(0px,_max-content)_minmax(0px,_max-content)_60px_minmax(0px,_max-content)_minmax(0px,_max-content)] fws-max-w-[1200px] fws-w-full md:fws-pt-4 fws-shadow-lg md:fws-rounded fws-overflow-x-hidden">

            <?php get_header() ?>

            <aside class="fws-hidden md:fws-block fws-col-start-1 fws-col-end-3 fws-row-start-3 fws-row-end-5">
                <div class="fws-bg-white fws-h-[17px] fws-mt-[13px] fws-mb-[-13px]">
                    <svg id="wave" style="transform:rotate(180deg); transition: 0.3s" viewBox="0 0 1440 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                        <defs>
                            <linearGradient id="sw-gradient-0" x1="0" x2="0" y1="1" y2="0">
                                <stop stop-color="rgba(255, 255, 255, 1)" offset="0%"></stop>
                                <stop stop-color="rgba(255, 255, 255, 1)" offset="100%"></stop>
                            </linearGradient>
                        </defs>
                        <path style="transform:translate(0, 0px); opacity:1" fill="url(#sw-gradient-0)" d="M0,80L120,66.7C240,53,480,27,720,13.3C960,0,1200,0,1440,15C1680,30,1920,60,2160,73.3C2400,87,2640,83,2880,73.3C3120,63,3360,47,3600,33.3C3840,20,4080,10,4320,18.3C4560,27,4800,53,5040,58.3C5280,63,5520,47,5760,41.7C6000,37,6240,43,6480,43.3C6720,43,6960,37,7200,38.3C7440,40,7680,50,7920,60C8160,70,8400,80,8640,85C8880,90,9120,90,9360,80C9600,70,9840,50,10080,43.3C10320,37,10560,43,10800,38.3C11040,33,11280,17,11520,11.7C11760,7,12000,13,12240,21.7C12480,30,12720,40,12960,41.7C13200,43,13440,37,13680,40C13920,43,14160,57,14400,66.7C14640,77,14880,83,15120,86.7C15360,90,15600,90,15840,78.3C16080,67,16320,43,16560,31.7C16800,20,17040,20,17160,20L17280,20L17280,100L17160,100C17040,100,16800,100,16560,100C16320,100,16080,100,15840,100C15600,100,15360,100,15120,100C14880,100,14640,100,14400,100C14160,100,13920,100,13680,100C13440,100,13200,100,12960,100C12720,100,12480,100,12240,100C12000,100,11760,100,11520,100C11280,100,11040,100,10800,100C10560,100,10320,100,10080,100C9840,100,9600,100,9360,100C9120,100,8880,100,8640,100C8400,100,8160,100,7920,100C7680,100,7440,100,7200,100C6960,100,6720,100,6480,100C6240,100,6000,100,5760,100C5520,100,5280,100,5040,100C4800,100,4560,100,4320,100C4080,100,3840,100,3600,100C3360,100,3120,100,2880,100C2640,100,2400,100,2160,100C1920,100,1680,100,1440,100C1200,100,960,100,720,100C480,100,240,100,120,100L0,100Z"></path>
                    </svg>
                </div>
                <defs>
                    <linearGradient id="sw-gradient-0" x1="0" x2="0" y1="1" y2="0">
                        <stop stop-color="rgba(255, 255, 255, 1)" offset="0%"></stop>
                        <stop stop-color="rgba(255, 255, 255, 1)" offset="100%"></stop>
                    </linearGradient>
                </defs>
                <path style="transform:translate(0, 0px); opacity:1" fill="url(#sw-gradient-0)" d="M0,80L120,66.7C240,53,480,27,720,13.3C960,0,1200,0,1440,15C1680,30,1920,60,2160,73.3C2400,87,2640,83,2880,73.3C3120,63,3360,47,3600,33.3C3840,20,4080,10,4320,18.3C4560,27,4800,53,5040,58.3C5280,63,5520,47,5760,41.7C6000,37,6240,43,6480,43.3C6720,43,6960,37,7200,38.3C7440,40,7680,50,7920,60C8160,70,8400,80,8640,85C8880,90,9120,90,9360,80C9600,70,9840,50,10080,43.3C10320,37,10560,43,10800,38.3C11040,33,11280,17,11520,11.7C11760,7,12000,13,12240,21.7C12480,30,12720,40,12960,41.7C13200,43,13440,37,13680,40C13920,43,14160,57,14400,66.7C14640,77,14880,83,15120,86.7C15360,90,15600,90,15840,78.3C16080,67,16320,43,16560,31.7C16800,20,17040,20,17160,20L17280,20L17280,100L17160,100C17040,100,16800,100,16560,100C16320,100,16080,100,15840,100C15600,100,15360,100,15120,100C14880,100,14640,100,14400,100C14160,100,13920,100,13680,100C13440,100,13200,100,12960,100C12720,100,12480,100,12240,100C12000,100,11760,100,11520,100C11280,100,11040,100,10800,100C10560,100,10320,100,10080,100C9840,100,9600,100,9360,100C9120,100,8880,100,8640,100C8400,100,8160,100,7920,100C7680,100,7440,100,7200,100C6960,100,6720,100,6480,100C6240,100,6000,100,5760,100C5520,100,5280,100,5040,100C4800,100,4560,100,4320,100C4080,100,3840,100,3600,100C3360,100,3120,100,2880,100C2640,100,2400,100,2160,100C1920,100,1680,100,1440,100C1200,100,960,100,720,100C480,100,240,100,120,100L0,100Z"></path>
                </svg>
                <div class="fws-h-full fws-w-full">
                    <?php
                    get_template_part('src/Theme/Layout/aside');
                    ?>
                </div>
            </aside>

            <main role="main" class="fws-relative fws-w-full fws-col-start-1 md:fws-col-start-3 fws-col-end-10 fws-row-start-3 md:fws-row-start-4 fws-row-end-5 fws-pt-16 fws-px-8 fws-pb-10 md:fws-pt-0 md:fws-pl-7 fws-z-0">
                <?php

                $isPostArchive = array_key_exists('type', $args) === true && $args['type'] === 'archive';

                if ($isPostArchive === true) {
                    $currentObjectId = $menu->getQueriedObjectId();
                    $currentMenuItem = $menu->findMenuItemByObjectId($currentObjectId);

                    // get width and height only one time and not each iteration. Its used in post-archive.php for $defaultThumbnail
                    $args['thumbnailWidth'] =  get_option('thumbnail_size_w') . 'px';
                    $args['thumbnailHeight'] =  get_option('thumbnail_size_h') . 'px';
                ?>
                    <?php get_template_part('src/Theme/Layout/page/page-heading', null, ['title' => $currentMenuItem->title]); ?>

                <?php
                }

                if (have_posts()) {
                    for ($iteration = 0; have_posts(); $iteration++) {

                        if ($isPostArchive) {
                            $args['isFirstPost'] = $iteration === 0 && $paged === 0;
                        }

                        the_post();

                        get_template_part('src/Theme/Layout/main', null, $args ?? null);
                    }
                }

                if ($args) {

                    // pagination for the post archive
                    if ($isPostArchive === true) {
                        $postPagination = (new Container())->getInstance()->get(PostPagination::class);
                        echo $postPagination->getPagination();
                    }
                }
                ?>

            </main>

            <div class="fws-row-start-5 fws-row-end-6 fws-col-start-1 fws-col-end-10">
                <?php get_footer() ?>
            </div>
        </div>
        <button data-fws-class="BackToTop" title="Zurück nach oben" class="fws-p-2 fws-hidden fws-rounded fws-fixed fws-bottom-14 fws-right-10 md:fws-right-16 fws-bg-gray-900 fws-z-10 fws-opacity-80 hover:fws-opacity-100" type="button">
            <div class="fws-flex fws-flex-col fws-justify-between fws-w-[20px] fws-h-[20px] fws-transform fws-origin-center -fws-translate-y-1.5 -fws-rotate-90">
                <div class="fws-bg-white fws-h-[2px] fws-origin-left fws-rotate-[42deg] fws-w-2/3"></div>
                <div class="fws-bg-white fws-h-[2px] fws-origin-left -fws-rotate-[42deg] fws-w-2/3"></div>
            </div>
        </button>
    </div>
</body>