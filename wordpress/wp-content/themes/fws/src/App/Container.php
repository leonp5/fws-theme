<?php

declare(strict_types=1);

namespace Leonp5\fws\App;

use function DI\factory;
use DI\ContainerBuilder;
use DI\Container as DIContainer;

use Leonp5\fws\Config\config;

final class Container
{
    /**
     * @var ?DIContainer
     */
    private static $instance = null;

    public static function getInstance(): DIContainer
    {
        if (self::$instance === null) {
            self::createInstance();
        }

        return self::$instance;
    }

    private static function createInstance(): void
    {
        $dependencyProvider = include_once(get_template_directory() . '/src/Config/dependencies.php');
        $definitions = [];

        /* @var $provider DependencyProviderInterface */
        foreach ($dependencyProvider as $provider) {
            $createdProvider = new $provider();
            foreach ($createdProvider->getAliases() as $class =>  $classFactory) {
                $definitions[$class] = factory([$classFactory, 'create']);
            }
        }

        $definitions = (new config())->set($definitions);

        $containerBuilder = new ContainerBuilder();
        $containerBuilder->useAutowiring(false);
        $containerBuilder->addDefinitions($definitions);

        self::$instance = $containerBuilder->build();
    }
}
