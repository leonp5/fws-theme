import Initializer from './components/Initializer/Initializer'

export default class App {
    run(): void {
        // Must defined before classes are initialized, so they can access this in the constructor
        globalThis.appShortCut = 'fws'

        const initializer = new Initializer()
        initializer.init()
    }
}
