<?php

declare(strict_types=1);

namespace Leonp5\fws\Theme\partials\Menu;

use Psr\Container\ContainerInterface;

use Leonp5\fws\App\Interfaces\FactoryInterface;

final class MenuFactory implements FactoryInterface
{
    public static function create(ContainerInterface $container): MenuInterface
    {
        return new Menu(
            $container->get('config')['defaultBgColor']
        );
    }
}
