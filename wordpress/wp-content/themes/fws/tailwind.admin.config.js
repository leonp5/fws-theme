/** @type {import('tailwindcss').Config} */
module.exports = {
    prefix: 'fws-admin-',
    content: ['./*.php', './**/*.php'],
    theme: {
        extend: {},
    },

    plugins: [],
}
