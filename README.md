# Docker setup for Wordpress theme development & Wordpress theme

## .env file

- The values from the `.env` file are used inside the `docker-compose.yaml` and inside the webpack config (`webpack.common.js`). Keep this in mind if you change values or if you move or delete the `.env` file.
  - **Notice:** If you add a env variable in the `.env` file and want it to be available inside docker you have to add it in the `docker-compose.yaml` too. For examples look in the `docker-compose.yaml` at the key `environment` in one of the services.
  - **Notice:** If you change the project name in the `.env` file you have to adopt it in the `launch.json` file too, if you use VSCode & XDebug. This counts also if you change the volume path for the project in the `docker-compose.yaml`.

## Setup docker & docker compose

- Install docker, docker compose and [mkcert](https://github.com/FiloSottile/mkcert)

  - Run `mkcert -install` (Creates a new local CA)

- In `/etc/hosts` on your local machine add `127.0.1.1 wp-theme.dev` (or the domain you want to use)

  - **Maybe** it is necessary to install `nscd` and then restart it with `sudo service nscd restart`

- For the database host name in the Wordpress database connection setup the service name from the docker-compose is used: `mysql`

- On a Linux machine it **might** be necessary to pass the user via CLI

  - Therefore you have to add the following lines to the docker compose file.

    - ```
        x-user: &user
          user: ${CURRENT_UID}

        ...

        services:
          php:
          <<: *user

        ...

          node:
          <<: *user
      ```

    - Pass the CURRENT_UID via CLI when starting docker compose: `CURRENT_UID=$(id -u):82 docker compose up -d --build`

## Setup Wordpress

- Download the current Wordpress version
- Create a folder inside the project folder. The name of the folder should be wordpress
- Copy the unpacked content of the downloaded Wordpress file into the wordpress folder in your project
- Start the wordpress setup in the browser by calling the URL or create the `wp-config-php` manually
- Inside `wordpress/wp-content/themes` create a folder for the theme you want to develop
- Exclude this foldername on the gitignore at the bottom like this `!wordpress/wp-content/themes/your_folder_name`
- The gitignore part for excluding the whole wordpress folder except your theme folder should look like this (it's a workaround)
- In the `wp-config.php` insert `define('FS_METHOD', 'direct');` to enable direct plugin installation (no FTP access). Create the folders `upgrades` and `plugins` in `wp-content` and chown them to `currentUser:82`.

  ```
  wordpress/*
  !wordpress/wp-content/

  wordpress/wp-content/*
  !wordpress/wp-content/themes

  wordpress/wp-content/themes/*
  !wordpress/wp-content/themes/your_folder_name

  wordpress/wp-content/themes/your_folder_name/node_modules
  ```

## Use VSCode Action Buttons Extension

- The [VSCode Action Buttons Extension](https://marketplace.visualstudio.com/items?itemName=jkearins.action-buttons-ext) adds custom buttons to the VSCode status bar
- There are a few buttons (which you can edit or remove) like one for `docker compose up -d` or `docker compose down`. You can add your own custom buttons if you want.

## PHP Autoload

- Composer is installed inside the PHP container. So if you want to install any packages or generat the autoload file, exec into the PHP container and run the required composer command.

  - Generate autoload files for the dev environment: `composer dump-autoload -o` (-optimize)

  - For prod (if you have dev dependencies): `composer install --no-dev --optimize-autoloader`

## Xdebug

- From my current state of knowledge the php-upstream port in the nginx conf and the Xdebug port must be the same. If you use VSCodium (or VSCode) that means in the `launch.json` the port must be the same as in the `.env` file resp. in the php-upstream port in the nginx conf.
- If you just cloned or downloaded the repository you find a working `launch.json` file in the `.vscode` folder.
- **Notice:** Disable your firewall during development or add an exception for the Xdebug port. Otherwise Xdebug won't be able to connect from the container to localhost.
  - For Linux that could mean, add a rule to your firewall (UFW for example) for incoming connections. Allow incoming connections on th Port you set for Xdebug.

## Permission handling on Linux

- The folder `wordpress/wp-content/uploads` should have the permissions `775`

---

### Docker on Linux in "Deutsche Bahn"

---

Deutsche Bahn WiFi & Docker using the same default IP.
To resolve the conflicts create a file `deamon.json` in `/etc/docker/` an add a alternative IP address.
Remove `docker0` and maybe other (bridge)-networks.

```
{
"bip":"172.32.1.1/24"
}
```

Restart docker with `sudo service docker restart` or `sudo systemctl restart docker.service`

---

## TODO

- Alter Server?
