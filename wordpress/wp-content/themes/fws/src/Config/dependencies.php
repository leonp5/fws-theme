<?php

declare(strict_types=1);

namespace Leonp5\fws\Config;

use Leonp5\fws\Functions\FunctionsDependencyProvider;
use Leonp5\fws\Theme\ThemeDependencyProvider;

return [
    FunctionsDependencyProvider::class,
    ThemeDependencyProvider::class,
];
