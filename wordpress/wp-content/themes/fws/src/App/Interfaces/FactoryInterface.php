<?php

declare(strict_types=1);

namespace Leonp5\fws\App\Interfaces;

use Psr\Container\ContainerInterface;

interface FactoryInterface
{
    public static function create(ContainerInterface $container);
}
