<?php

namespace Leonp5\fws\Functions\AddWidgetActions;

use Leonp5\fws\Functions\AddActionsInterface;

interface AddWidgetActionsInterface extends AddActionsInterface
{
    public function addWidgetAreas(): void;
}
