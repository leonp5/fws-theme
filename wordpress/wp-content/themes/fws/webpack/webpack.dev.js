const path = require("path");
const postCssPresetEnv = require("postcss-preset-env");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env) => {
    const envVariables = require("dotenv").config({
        path: path.resolve(__dirname, env.envFilePath),
    }).parsed;

    return {
        mode: "development",
        devtool: "eval",
        watchOptions: {
            ignored: /node_modules/,
        },

        devServer: {
            port: envVariables.WEBPACK_SERVER_PORT,
            allowedHosts: [`${envVariables.APP_DOMAIN}`],
            devMiddleware: {
                // writeToDisk: true,
                // avoid copy all the temporary files
                writeToDisk: (filePath) => {
                    return /^(?!.*(hot)).*/.test(filePath);
                },
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
            },
            liveReload: true,
            hot: true,
            watchFiles: {
                paths: ["./*.php", "./**/*.php"],
            },
            static: {
                directory: path.join(__dirname, "../dist/"),
            },

            client: {
                webSocketURL: `https://${envVariables.APP_DOMAIN}/ws`,
            },
        },

        module: {
            rules: [
                {
                    test: /\.ejs$/,
                    loader: "ejs-loader",
                    options: {
                        esModule: false,
                    },
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        {
                            // apply postCSS fixes like autoprefixer
                            loader: "postcss-loader",
                            options: {
                                postcssOptions: {
                                    plugins: [
                                        require("tailwindcss"),
                                        require.resolve("autoprefixer"),
                                        // for different browsers
                                        postCssPresetEnv({
                                            browsers: "last 2 versions",
                                        }),
                                    ],
                                },
                            },
                        },
                        "sass-loader",
                    ],
                },
            ],
        },

        plugins: [],
    };
};
