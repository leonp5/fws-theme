<?php

declare(strict_types=1);

namespace Leonp5\fws\Functions\AddAssetActions;

use Psr\Container\ContainerInterface;

use Leonp5\fws\App\Interfaces\FactoryInterface;

final class AddAssetActionsFactory implements FactoryInterface
{
    public static function create(ContainerInterface $container): AddAssetsActionsInterface
    {
        return new AddAssetActions(
            $container->get('config')['env'],
            $container->get('config')['projectName']
        );
    }
}
