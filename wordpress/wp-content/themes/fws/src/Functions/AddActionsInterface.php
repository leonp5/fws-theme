<?php

declare(strict_types=1);

namespace Leonp5\fws\Functions;

interface AddActionsInterface
{
    public function addActions(): void;
}
