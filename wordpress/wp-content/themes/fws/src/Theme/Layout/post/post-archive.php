<?php

declare(strict_types=1);

/**
 * Template post archive
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Theme\partials\Menu\Menu;

$container = (new Container())->getInstance();
$menu = $container->get(Menu::class);

?>

<a class="fws-flex fws-flex-col fws-mb-7 fws-post-shadow" href="<?php the_permalink(); ?>">
    <div class="fws-flex fws-items-center">
        <?php

        $defaultThumbnail = $container->get('config')['assetUri'] . '/images/fwsm_ar_logo_small.jpg';

        if (has_post_thumbnail() === true) {
        ?>
            <img src="<?php the_post_thumbnail_url('thumbnail') ?>">
        <?php
        } else {
        ?>
            <img style="width: <?php echo $args['thumbnailWidth'] ?>; height: <?php echo $args['thumbnailHeight'] ?>" src="<?php echo $defaultThumbnail ?>">
        <?php

        }

        ?>
        <div class="fws-flex fws-flex-col fws-p-3">
            <h2 class="fws-text-lg fws-text-gray-700 fws-mb-1 fws-font-semibold"><?php the_title() ?></h2>
            <span class="fws-text-gray-400 fws-font-semibold fws-text-xs fws-mb-5"><?php the_date(); ?></span>
            <div class="fws-leading-6 fws-text-gray-700"><?php echo the_excerpt(); ?></div>
        </div>
    </div>
</a>

<?php

if ($args['isFirstPost'] === true) {
?>
    <div class="fws-h-[3px] fws-w-full fws-mb-7" style="background: <?php echo $menu->getCurrenPageBgColor() ?>;"></div>
<?php
}
