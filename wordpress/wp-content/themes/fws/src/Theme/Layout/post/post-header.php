<?php

declare(strict_types=1);

/**
 * Template post header
 *
 * @package fws_melle_theme
 */

?>
<header class="fws-mb-10">
    <div class="fws-flex fws-flex-col">
        <div class="fws-flex">
            <span class="fws-text-gray-600 fws-font-semibold fws-text-xs fws-mr-1">Veröffentlicht:</span>
            <span class="fws-text-gray-400 fws-font-semibold fws-text-xs"><?php the_date();  ?></span>
        </div>
        <?php
        $tags = get_the_tags();
        $numberOfTags = $tags === false ? 0 : count($tags);

        if ($numberOfTags > 0) {
        ?>

            <div class="fws-flex fws-items-center fws-mt-1">
                <span class="fws-text-gray-600 fws-font-semibold fws-text-xs fws-mr-1">Schlagwörter:</span>
                <?php
                $i = 1;
                foreach ($tags as $tag) {
                    $tagName = $i < $numberOfTags ? $tag->name . ", " : $tag->name;

                    echo '<i class="gg-tag fws-text-gray-400 fws-mt-1 fws-mr-1" style="--ggs: 0.7;"></i> <span class="fws-m-0 fws-text-gray-400 fws-font-semibold fws-text-xs">' .
                        $tagName .  '</span>';
                    $i++;
                }
                ?>
            </div>
        <?php
        }
        ?>

    </div>

</header>