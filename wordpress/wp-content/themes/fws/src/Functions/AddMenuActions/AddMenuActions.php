<?php

declare(strict_types=1);

namespace Leonp5\fws\Functions\AddMenuActions;

use WP_Post;

use Leonp5\fws\Theme\partials\Menu\MenuInterface;

final class AddMenuActions implements AddMenuActionsInterface
{
    private MenuInterface $menu;
    private string $defaultBgColor;
    private int $checkedReferencePageCount = 0;
    private bool $eventCalendarPageUpdated = false;

    /**
     * @var array<int>
     */
    private array $uncheckedReferencePagePostIds = [];

    public function __construct(
        MenuInterface $menu,
        string $defaultBgColor
    ) {
        $this->menu = $menu;
        $this->defaultBgColor = $defaultBgColor;
    }

    public function addActions(): void
    {
        add_action('init', [$this, 'addMenus']);
        add_action('wp_nav_menu_item_custom_fields', [$this, 'showAdditionalMenuItemInputs'], 10, 2);
        add_action('wp_update_nav_menu_item', [$this, 'saveMenuItemBgColor'], 10, 2);
        add_action('wp_update_nav_menu_item', [$this, 'saveEventCalendarReferencePage'], 10, 2);
    }

    public function addMenus(): void
    {
        $menus = [
            'primary' => 'Hauptmenü',
            'footer' => 'Footer Menü'
        ];

        register_nav_menus($menus);
    }

    public function showAdditionalMenuItemInputs(int $item_id, WP_POST $menuItem): void
    {
        $postMeta = get_post_meta($item_id);

        $checked = $postMeta['event_calendar_reference_page'][0] === '1' ? 'checked' : '';

        if ($menuItem->menu_item_parent === '0') {
?>
            <div class="fws-admin-mb-3" style="clear: both;">
                <span class="description"><?php _e("Hintergrundfarbe des Menüpunkts", 'bg_color'); ?></span><br />
                <input type="hidden" class="nav-menu-id" value="<?php echo $item_id; ?>" />
                <div class="logged-input-holder">
                    <input type="color" name="bg_color[<?php echo $item_id; ?>]" id="menu-item-bg-color-<?php echo $item_id; ?>" value="<?php echo esc_attr($postMeta['_bg_color'][0]); ?>" />
                </div>
            </div>
        <?php
        }

        ?>
        <div class="fws-admin-flex fws-admin-mb-3" style="clear: both;">
            <div class="logged-input-holder fws-admin-mr-3">
                <input type="checkbox" name="event_calendar_reference_page[<?php echo $item_id; ?>]" id="menu-item-calendar-reference-<?php echo $item_id; ?>" <?php echo $checked; ?> />
            </div>
            <span class="description">Häkchen setzen, wenn diese Seite die Referenz Seite für den Event Kalender sein soll. Die Event Kalender Übersicht und Einzelansicht werden als Teil dieses (Unter)-Menüs behandelt. Das heißt, Hintergrundfarbe und weitere Menüpunkte werden mit Bezug auf diese Seite bei den Kalender Seiten angezeigt. <br> <strong>Achtung: </strong>Das Häkchen kann nur bei einer Seite gesetzt sein. Sollte es bei einer anderen Seite bereits gesetzt sein, wird beim Speichern nur das erste (von oben nach unten) gesetzte Häkchen gespeichert.</strong></span><br />
            <input type="hidden" class="nav-menu-id" value="<?php echo $item_id; ?>" />
        </div>
<?php
    }

    public function saveMenuItemBgColor(int $menuId, int $postId): void
    {
        $bgColor = $this->defaultBgColor;

        if (isset($_POST['bg_color'][$postId])) {
            $sanitized_data = sanitize_text_field($_POST['bg_color'][$postId]);
            update_post_meta($postId, '_bg_color', $sanitized_data);
            $bgColor = $sanitized_data;
        }

        $menuKey = $this->menu->findMenuLocationKeyByMenuId($menuId);

        if ($menuKey !== null) {
            if ($this->menu->hasSubmenu($postId, $menuKey) === true) {
                $allMenuItems = $this->menu->getMenuItemsByKey($menuKey);

                foreach ($allMenuItems as $menuItem) {
                    if ((int) $menuItem->menu_item_parent === $postId) {
                        update_post_meta($menuItem->ID, '_bg_color', $bgColor);
                    }
                }
            }
        }
    }

    public function saveEventCalendarReferencePage(int $menuId, int $postId): void
    {
        // only one page could be the reference page for the event calendar
        if (isset($_POST['event_calendar_reference_page'][$postId]) && $this->checkedReferencePageCount < 1) {
            update_post_meta($postId, 'event_calendar_reference_page', 1);
            $this->checkedReferencePageCount++;
        } else {
            update_post_meta($postId, 'event_calendar_reference_page', 0);
        }
    }
}
