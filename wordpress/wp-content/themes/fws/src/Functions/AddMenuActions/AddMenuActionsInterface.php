<?php

namespace Leonp5\fws\Functions\AddMenuActions;

use WP_Post;

use Leonp5\fws\Functions\AddActionsInterface;

interface AddMenuActionsInterface extends AddActionsInterface
{
    public function addMenus(): void;

    public function saveMenuItemBgColor(int $menuId, int $menuItemDbId): void;

    public function showAdditionalMenuItemInputs(int $item_id, WP_POST $menuItem): void;

    public function saveEventCalendarReferencePage(int $menuId, int $menuItemDbId): void;
}
