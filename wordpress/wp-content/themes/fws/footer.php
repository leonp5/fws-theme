<?php

declare(strict_types=1);

/**
 * Footer template
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Theme\partials\Menu\Menu;

$container = (new Container())->getInstance();
$menu = $container->get(Menu::class);
?>

<div class="fws-mt-[-30px] fws-h-[50px]">
    <svg viewBox="0 30 500 60" preserveAspectRatio="none" style="height: 100%; width: 100%;">
        <path d="M-3.10,82.40 C153.78,44.90 226.01,136.67 500.27,42.92 L501.40,152.47 L0.00,150.00 Z" style="stroke: none; fill: <?php echo $menu->getCurrenPageBgColor() ?>; background: <?php echo $menu->getCurrenPageBgColor() ?>;"></path>
    </svg>
</div>
<footer class="fws-flex">
    <div style="background: <?php echo $menu->getCurrenPageBgColor() ?>;" class="fws-relative fws-flex fws-justify-end fws-w-full fws-min-h-[75px] md:fws-rounded-b">
        <div class="fws-absolute fws-bottom-0 fws-left-0 md:fws-left-[-10px]">
            <img class="fws-w-[100px] md:fws-w-[200px] lg:fws-w-[250px]" src="<?php echo $container->get('config')['assetUri'] . '/images/Heckenrose_rechts-3-min.png' ?>" alt="" />
        </div>
        <div class="fws-flex fws-flex-wrap fws-items-end fws-pb-4 fws-pr-4 fws-pl-28">
            <?php
            $footerMenuItems = $menu->getFirstOrderMenuItemsByKey('footer');
            foreach ($footerMenuItems as $menuItem) {
            ?>
                <a class="fws-mx-2 hover:fws-opacity-80 fws-text-white fws-font-semibold" href="<?php echo esc_url($menuItem->url); ?>" title="<?php echo esc_attr($menuItem->title); ?>">
                    <?php echo esc_html($menuItem->title); ?>
                </a>
            <?php
            }
            ?>
        </div>
    </div>
    <?php
    wp_footer()
    ?>
</footer>