<?php

namespace Leonp5\fws\Config;

class config
{
    public function set(array $definitions): array
    {
        $config = [];

        $appEnv = (string) getenv('APP_ENV') !== ''
            ? (string) getenv('APP_ENV') : 'prod';

        $config['env'] = $appEnv;

        $config['projectName'] = (string) getenv('PROJECT_NAME') !== ''
            ? (string) getenv('PROJECT_NAME') : 'fws';

        $assetUri = $appEnv === 'dev' ? '/' . $config['projectName'] . '/assets'
            : get_template_directory_uri() . '/dist';

        $config['assetUri'] = $assetUri;

        // default background color
        $defaultBgColor = (string) getenv('DEFAULT_BG_COLOR') !== ''
            ? (string) getenv('DEFAULT_BG_COLOR') : '#e84442';

        $config['defaultBgColor'] = $defaultBgColor;

        $definitions['config'] = $config;

        return $definitions;
    }
}
