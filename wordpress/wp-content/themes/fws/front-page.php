<?php

declare(strict_types=1);

/**
 * Template front-page
 *
 * @package fws_melle_theme
 */


get_template_part('src/Theme/Layout/page/page');
