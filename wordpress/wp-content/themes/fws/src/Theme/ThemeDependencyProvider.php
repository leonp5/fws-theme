<?php

namespace Leonp5\fws\Theme;

use Leonp5\fws\Theme\partials\Menu\Menu;
use Leonp5\fws\Theme\partials\Menu\MenuFactory;
use Leonp5\fws\App\Interfaces\DependencyProviderInterface;
use Leonp5\fws\Theme\partials\PostPagination\PostPagination;
use Leonp5\fws\Theme\partials\PostPagination\PostPaginationFactory;

class ThemeDependencyProvider implements DependencyProviderInterface
{
    public function getAliases(): array
    {
        return [
            Menu::class => MenuFactory::class,
            PostPagination::class => PostPaginationFactory::class
        ];
    }
}
