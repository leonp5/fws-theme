<?php

declare(strict_types=1);

namespace Leonp5\fws\Functions;

use Leonp5\fws\Functions\AddMenuActions\AddMenuActions;
use Leonp5\fws\Functions\AddAssetActions\AddAssetActions;
use Leonp5\fws\App\Interfaces\DependencyProviderInterface;
use Leonp5\fws\Functions\AddWidgetActions\AddWidgetActions;
use Leonp5\fws\Functions\AddMenuActions\AddMenuActionsFactory;
use Leonp5\fws\Functions\AddAssetActions\AddAssetActionsFactory;
use Leonp5\fws\Functions\AddWidgetActions\AddWidgetActionsFactory;

final class FunctionsDependencyProvider implements DependencyProviderInterface
{
    public function getAliases(): array
    {
        return [
            AddMenuActions::class => AddMenuActionsFactory::class,
            AddAssetActions::class => AddAssetActionsFactory::class,
            AddWidgetActions::class => AddWidgetActionsFactory::class,
        ];
    }
}
