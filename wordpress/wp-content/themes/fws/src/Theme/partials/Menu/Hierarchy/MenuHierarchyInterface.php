<?php

namespace Leonp5\fws\Theme\partials\Menu\Hierarchy;

class MenuHierarchyInterface
{
    public const MENU_HIERARCHY_ONE = 1;
    public const MENU_HIERARCHY_TWO = 2;
    public const MENU_HIERARCHY_THREE = 3;
}
