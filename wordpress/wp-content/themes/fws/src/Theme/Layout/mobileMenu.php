<?php

declare(strict_types=1);

/**
 * Template mobile menu
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Theme\partials\Menu\Menu;

$container = (new Container())->getInstance();

?>

<button class="fws-flex fws-flex-col fws-justify-between fws-w-[20px] fws-h-[20px] fws-transform fws-transition-all fws-duration-300 fws-origin-center fws-overflow-hidden">
    <div class="fws-bg-white fws-h-[2px] fws-w-7 fws-transform fws-transition-all fws-duration-300 fws-origin-left"></div>
    <div class="fws-bg-white fws-h-[2px] fws-w-7 fws-rounded fws-transform fws-transition-all fws-duration-300 fws-delay-75"></div>
    <div class="fws-bg-white fws-h-[2px] fws-w-7 fws-transform fws-transition-all fws-duration-300 fws-origin-left fws-delay-150"></div>

    <div id="fws-hamburger-trans-animation-wrapper" class="fws-absolute fws-flex fws-items-center fws-justify-between fws-transform fws-transition-all fws-duration-500 fws-top-2.5 -fws-translate-x-10 fws-w-0">
        <div class="fws-absolute fws-bg-white fws-h-[2px] fws-w-5 fws-transform fws-transition-all fws-duration-500 fws-rotate-0 fws-delay-300"></div>
        <div class="fws-absolute fws-bg-white fws-h-[2px] fws-w-5 fws-transform fws-transition-all fws-duration-500 -fws-rotate-0 fws-delay-300"></div>
    </div>
</button>

<div id="fws-mobile-menu" class="fws-hidden fws-overflow-auto fws-absolute fws-w-full fws-z-20 fws-bg-white fws-transition-all fws-opacity-0 fws-duration-300">
    <nav class="fws-flex fws-flex-col fws-w-full fws-h-full">
        <?php
        $menu = $container->get(Menu::class);
        $locationKey = 'primary';
        $primaryMenuItems = $menu->getFirstOrderMenuItemsByKey($locationKey);
        foreach ($primaryMenuItems as $menuItem) {
            $withSubmenu = $menu->hasSubmenu($menuItem->ID, $locationKey);

        ?>
            <div class="fws-grid fws-grid-cols-6 fws-grid-rows-[50px_minmax(0px,_1fr)] fws-justify-center fws-items-center fws-w-full">
                <a class="fws-col-start-2 fws-col-span-4 fws-py-[0.4rem] hover:fws-opacity-80 fws-text-gray-900 fws-text-center fws-font-semibold" href="<?php echo esc_url($menuItem->url); ?>" title="<?php echo esc_attr($menuItem->title); ?>">
                    <?php echo esc_html($menuItem->title); ?>
                </a>

                <?php

                if ($withSubmenu === true) {
                    $submenuItems = $menu->findSubemenuItems($menuItem->ID, $locationKey);
                ?>
                    <button class="fws-py-3 fws-pl-2 fws-ml-2 fws-col-start-6" type="button" data-fws-class="ToggleSubmenu" data-fws-submenu-toggle="<?php echo $menuItem->ID ?>" data-fws-submenu-open="false" data-fws-rotate="false">
                        <div class="fws-flex fws-flex-col fws-justify-between fws-w-[15px] fws-h-[15px] fws-transform fws-transition-all fws-duration-300 fws-origin-center fws-overflow-hidden -fws-rotate-90">
                            <div class="fws-bg-gray-900 fws-h-[2px] fws-origin-right -fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                            <div class="fws-bg-gray-900 fws-h-[2px] fws-origin-right fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                        </div>
                    </button>

                    <ul class="fws-max-h-0 fws-opacity-0 fws-overflow-auto fws-flex fws-flex-col fws-justify-center fws-items-center fws-col-start-1 fws-col-end-7 fws-transform fws-transition-all fws-duration-300" style="background: <?php echo $menuItem->bg_color ?>;" data-fws-submenu="<?php echo $menuItem->ID ?>">

                        <?php

                        foreach ($submenuItems as $submenuItem) {
                            $withSubSubmenu = $menu->hasSubmenu($submenuItem->ID, $locationKey);

                        ?>
                            <div class="fws-grid fws-grid-cols-6 fws-grid-rows-[minmax(0px,_max-content)_minmax(0px,_max-content)] fws-justify-center fws-items-center fws-w-full">
                                <li class="fws-col-start-2 fws-col-span-4 fws-row-start-1 fws-row-end-2 fws-text-center fws-py-3">
                                    <a class="hover:fws-opacity-80 fws-text-white fws-font-semibold" href="<?php echo esc_url($submenuItem->url); ?>" title="<?php echo esc_attr($submenuItem->title); ?>">
                                        <?php echo esc_html($submenuItem->title); ?>
                                    </a>
                                </li>

                                <?php

                                if ($withSubSubmenu === true) {
                                    $subSubMenuItems = $menu->findSubemenuItems($submenuItem->ID, $locationKey);

                                ?>
                                    <button class="fws-col-start-6 fws-justify-self-end" type="button" data-fws-class="ToggleSubmenu" data-fws-submenu-toggle="<?php echo 'mobile-sub-' . $submenuItem->ID ?>" data-fws-submenu-open="false" data-fws-rotate="true">
                                        <div class="fws-flex fws-flex-col fws-justify-between fws-w-[15px] fws-h-[15px] fws-transform fws-transition-all fws-duration-300 fws-origin-center fws-overflow-hidden fws-rotate-180">
                                            <div class="fws-bg-white fws-h-[2px] fws-origin-right -fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                                            <div class="fws-bg-white fws-h-[2px] fws-origin-right fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                                        </div>
                                    </button>

                                    <ul class="fws-border-l-2 fws-max-h-0 fws-opacity-0 fws-pl-5 fws-col-start-3 fws-col-end-7 fws-row-start-2 fws-row-end-3 fws-overflow-auto fws-flex-col fws-justify-center fws-items-center fws-transform fws-transition-all fws-duration-300" data-fws-submenu="<?php echo 'mobile-sub-' . $submenuItem->ID ?>" style="background: <?php echo $menuItem->bg_color ?>;">

                                        <?php

                                        foreach ($subSubMenuItems as $subSubMenuItem) {
                                        ?>
                                            <li class="fws-my-2 hover:fws-underline fws-underline-offset-4 fws-text-white">
                                                <a class="fws-text-left fws-font-semibold" href="<?php echo esc_url($subSubMenuItem->url); ?>" title="<?php echo esc_attr($subSubMenuItem->title); ?>">
                                                    <?php echo esc_html($subSubMenuItem->title); ?>
                                                </a>

                                            </li>

                                        <?php
                                        }
                                        ?>
                                    </ul>

                                <?php

                                }

                                ?>


                            </div>
                        <?php


                        }

                        ?>


                    </ul>

                <?php
                }

                ?>

            </div>
        <?php
        }
        ?>

    </nav>
</div>