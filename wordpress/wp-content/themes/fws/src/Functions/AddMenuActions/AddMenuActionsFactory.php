<?php

declare(strict_types=1);

namespace Leonp5\fws\Functions\AddMenuActions;

use Psr\Container\ContainerInterface;

use Leonp5\fws\Theme\partials\Menu\Menu;
use Leonp5\fws\App\Interfaces\FactoryInterface;

final class AddMenuActionsFactory implements FactoryInterface
{
    public static function create(ContainerInterface $container): AddMenuActionsInterface
    {
        return new AddMenuActions(
            $container->get(Menu::class),
            $container->get('config')['defaultBgColor']
        );
    }
}
