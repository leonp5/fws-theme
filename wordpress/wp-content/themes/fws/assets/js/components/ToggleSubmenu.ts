import InitialClassInterface from '../global/interfaces/InitialClassInterface'

export default class ToggleSubmenu implements InitialClassInterface {
    init(toggleBtnElement: HTMLButtonElement): void {
        toggleBtnElement.addEventListener('click', () => {
            const identifier = toggleBtnElement.getAttribute(
                `data-${globalThis.appShortCut}-submenu-toggle`
            )
            this.#toggleSubMenuState(toggleBtnElement)
            this.#toggleSubMenuArrowStyles(toggleBtnElement)
            this.#toggleSubMenuStyles(identifier)
        })
    }

    #toggleSubMenuStyles(identifier: string | null): void {
        if (identifier !== null) {
            const subMenu = document.querySelector(
                `[data-${globalThis.appShortCut}-submenu="${identifier}"]`
            ) as HTMLDivElement

            console.log(subMenu)

            if (subMenu !== null) {
                // show subMenu
                if (subMenu.classList.contains('fws-opacity-0') === true) {
                    this.#setSubMenuShowStyles(subMenu)

                    setTimeout(() => {
                        subMenu.classList.remove('fws-opacity-0')
                    }, 75)
                }

                // hide subMenu
                if (subMenu.classList.contains('fws-opacity-0') === false) {
                    subMenu.classList.add('fws-opacity-0')

                    setTimeout(() => {
                        this.#removeSubMenuShowStyles(subMenu)
                    }, 75)
                }
            }
        }
    }

    #removeSubMenuShowStyles(submenu: HTMLDivElement): void {
        submenu.classList.remove('fws-h-auto')
        submenu.classList.add('fws-max-h-0')
        submenu.classList.remove('fws-p-5')
    }

    #setSubMenuShowStyles(submenu: HTMLDivElement): void {
        submenu.classList.add('fws-h-auto')
        submenu.classList.remove('fws-max-h-0')
        submenu.classList.add('fws-p-5')
    }

    #toggleSubMenuState(toggleBtnElement: HTMLButtonElement): void {
        let state = toggleBtnElement.getAttribute(
            `data-${globalThis.appShortCut}-submenu-open`
        )

        if (state === 'false') {
            state = 'true'
        } else {
            state = 'false'
        }

        toggleBtnElement.setAttribute(
            `data-${globalThis.appShortCut}-submenu-open`,
            state
        )
    }

    #toggleSubMenuArrowStyles(toggleBtnElement: HTMLButtonElement): void {
        const rotateArrow = toggleBtnElement.getAttribute(
            `data-${globalThis.appShortCut}-rotate`
        )

        console.log(rotateArrow)

        const arrowElements = Array.from(
            toggleBtnElement.querySelectorAll('div')
        )

        if (rotateArrow === 'false') {
            arrowElements[1].classList.toggle('fws-rotate-[42deg]')
            arrowElements[1].classList.toggle('-fws-rotate-[42deg]')
            arrowElements[1].classList.toggle('fws-origin-left')
            arrowElements[1].classList.toggle('fws-origin-right')
            arrowElements[2].classList.toggle('fws-rotate-[42deg]')
            arrowElements[2].classList.toggle('-fws-rotate-[42deg]')
            arrowElements[2].classList.toggle('fws-origin-left')
            arrowElements[2].classList.toggle('fws-origin-right')
        } else {
            arrowElements[0].classList.toggle('fws-rotate-180')
            arrowElements[0].classList.toggle('fws-rotate-[270deg]')
        }
    }
}
