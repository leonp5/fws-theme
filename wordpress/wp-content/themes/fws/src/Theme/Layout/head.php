<?php

declare(strict_types=1);

/**
 * Template head
 *
 * @package fws_melle_theme
 */

?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    wp_head()
    ?>
</head>