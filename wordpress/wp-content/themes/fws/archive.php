<?php

declare(strict_types=1);

/**
 * Template single (Post)
 *
 * @package fws_melle_theme
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_template_part('src/Theme/Layout/head'); ?>

<?php get_template_part('src/Theme/Layout/body', null, ['type' => 'archive']); ?>

</html>