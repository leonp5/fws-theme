<?php

declare(strict_types=1);

/**
 * Template page
 *
 * @package fws_melle_theme
 */
?>

<?php
if ($args) {

    // Blog archive
    if (array_key_exists('type', $args) === true && $args['type'] === 'archive') {

        get_template_part('src/Theme/Layout/post/post-archive', null, $args);

        return;
    }

    // for single blog post add header
    if (array_key_exists('type', $args) === true && $args['type'] === 'post') {

        get_template_part('src/Theme/Layout/post/post-header');
    }
}

?>


<?php get_template_part('src/Theme/Layout/page/page-heading', null, ['title' => the_title('', '', false)]); ?>

<div class="fws-text-gray-700 fws-leading-7">

    <?php the_content(); ?>
</div>
