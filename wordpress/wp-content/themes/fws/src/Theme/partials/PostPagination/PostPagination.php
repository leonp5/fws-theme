<?php

declare(strict_types=1);

namespace Leonp5\fws\Theme\partials\PostPagination;

final class PostPagination implements PostPaginationInterface
{
    private string $currentPageBgColor;

    public function __construct(string $currentPageBgColor)
    {
        $this->currentPageBgColor = $currentPageBgColor;
    }

    public function getPagination(): ?string
    {
        if ($GLOBALS['wp_query']->max_num_pages <= 1) {
            return null;
        };

        $args = [];

        $args = wp_parse_args($args, [
            'mid_size' => 2,
            'prev_next' => false,
            'type' => 'plain'
        ]);

        $links = paginate_links($args);
        $prev_link = $this->getPreviousElement($links);
        $next_link = $this->getNextElement($links);
        $template  = '<nav class="fws-flex fws-flex-wrap fws-my-5 fws-pagination" role="navigation" style="--fws-background: %1$s;">
          %2$s%3$s%4$s
        </nav>';

        return sprintf($template, $this->currentPageBgColor, $prev_link, $links, $next_link);
    }

    private function getPreviousElement(string $links): ?string
    {
        if (str_starts_with($links, '<span')) {
            return null;
        }

        $prev_link = get_previous_posts_page_link();

        $template = '<a href="%1$s">Vorherige</a>';

        return sprintf($template, $prev_link);
    }

    private function getNextElement(string $links): ?string
    {
        if (str_ends_with($links, '</span>')) {
            return null;
        }

        $prev_link = get_next_posts_page_link();

        $template = '<a href="%1$s">Nächste</a>';

        return sprintf($template, $prev_link);
    }
}
