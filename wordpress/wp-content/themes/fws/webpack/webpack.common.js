const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = (env) => {
    const envVariables = require('dotenv').config({
        path: path.resolve(__dirname, env.envFilePath),
    }).parsed

    return {
        target: 'web',
        context: path.resolve(__dirname, '../assets'),

        entry: {
            app: ['./index.ts', './index.scss'],
            admin: ['./admin.scss'],
        },
        output: {
            libraryTarget: 'umd',
            libraryExport: 'default',
            path: path.resolve(__dirname, '../dist'),
            filename: `${envVariables.PROJECT_NAME}.[name].js`,
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    loader: 'ts-loader',
                },
                {
                    test: /\.(js)$/,
                    exclude: /node_modules/,
                    use: ['babel-loader'],
                },
            ],
        },
        resolve: {
            extensions: ['.ejs', '.tsx', '.ts', '.js'],
        },
        plugins: [
            new CopyWebpackPlugin({
                patterns: [
                    {
                        from: './images',
                        to: 'images/',
                    },
                ],
            }),

            new MiniCssExtractPlugin({
                filename: `${envVariables.PROJECT_NAME}.[name].css`,
            }),
        ],
    }
}
