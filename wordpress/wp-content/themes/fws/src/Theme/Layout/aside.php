<?php

declare(strict_types=1);

/**
 * Template aside
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Theme\partials\Menu\Hierarchy\MenuHierarchyInterface;
use Leonp5\fws\Theme\partials\Menu\Menu;

$container = (new Container())->getInstance();
$menu = $container->get(Menu::class);

?>

<div class="fws-w-full fws-h-full fws-pb-56" style="background: <?php echo $menu->getCurrenPageBgColor() ?>;">

    <nav class="fws-px-8 fws-py-10 fws-flex fws-flex-col fws-w-full fws-h-full" id="fws-submenu-width-reference">
        <?php
        if (is_singular('post') === true) {
            // if it is post, get the id of the blog archive page
            $currentObjectId = (int) get_option('page_for_posts');
        } else {
            $currentObjectId = $menu->getQueriedObjectId();
        }

        // 0 comes from The Events Calendar plugin for example
        if ($currentObjectId === 0 || $currentObjectId === null) {
            return;
        }

        $currentMenuItem = $menu->findMenuItemByObjectId($currentObjectId);

        if ($currentMenuItem === null) {
            return;
        }

        $parentMenuItemId = (int) $currentMenuItem->menu_item_parent !== 0 ? (int) $currentMenuItem->menu_item_parent : $currentMenuItem->ID;
        $menuItemHierarchy = $menu->determineMenuItemHierarchy($currentMenuItem->ID);

        $locationKey = $menu->findMenuLocationKeyByMenuItemId($currentMenuItem->ID) ?? 'primary';

        $submenuItems = $menu->findSubemenuItems($parentMenuItemId, $locationKey);

        if ($menuItemHierarchy === MenuHierarchyInterface::MENU_HIERARCHY_THREE) {
            $secondHierarchyParent = $menu->findMenuItemByMenuItemId((int) $currentMenuItem->menu_item_parent);

        ?>
            <a class="fws-w-full fws-flex fws-items-center fws-mb-5 hover:fws-opacity-80 fws-text-white" href="<?php echo esc_url($secondHierarchyParent->url); ?>" title="zurück zu <?php echo esc_attr($secondHierarchyParent->title); ?>">
                <div class="fws-pl-1">
                    <div class="fws-flex fws-flex-col fws-justify-between fws-w-[15px] fws-h-[15px] fws-transform fws-transition-all fws-duration-300 fws-origin-center fws-overflow-hidden">
                        <div class="fws-bg-white fws-h-[2px] fws-origin-right -fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                        <div class="fws-bg-white fws-h-[2px] fws-origin-right fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                    </div>
                </div>

                <span class="fws-text-left fws-font-semibold fws-ml-2">
                    <?php echo esc_html($secondHierarchyParent->title); ?>
                </span>
            </a>
        <?php

        }

        foreach ($submenuItems as $submenuItem) {
            $withSubSubmenu = $menu->hasSubmenu($submenuItem->ID, $locationKey);


        ?>
            <div class="fws-grid fws-grid-cols-6 fws-grid-rows-[minmax(0px,_max-content)_minmax(0px,_max-content)] fws-justify-center fws-items-center fws-w-full fws-py-[0.8rem]">
                <a class="fws-col-start-1 fws-col-end-6 fws-row-start-1 fws-row-end-2 fws-text-left hover:fws-opacity-80 fws-text-white fws-font-semibold fws-text-sm lg:fws-text-base" href="<?php echo esc_url($submenuItem->url); ?>" title="<?php echo esc_attr($submenuItem->title); ?>">
                    <?php echo esc_html($submenuItem->title); ?>
                </a>

                <?php

                if ($withSubSubmenu === true) {
                    $subSubMenuItems = $menu->findSubemenuItems($submenuItem->ID, $locationKey);
                ?>
                    <button class="fws-py-3 fws-pl-2 fws-pr-6 fws-ml-2 fws-col-start-6" type="button" data-fws-class="ToggleSubmenu" data-fws-submenu-toggle="<?php echo $submenuItem->ID ?>" data-fws-submenu-open="false" data-fws-rotate="true">
                        <div class="fws-flex fws-flex-col fws-justify-between fws-w-[15px] fws-h-[15px] fws-transform fws-transition-all fws-duration-300 fws-origin-center fws-overflow-hidden fws-rotate-180">
                            <div class="fws-bg-white fws-h-[2px] fws-origin-right -fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                            <div class="fws-bg-white fws-h-[2px] fws-origin-right fws-rotate-[42deg] fws-w-2/3 fws-transform fws-transition-all fws-duration-300"></div>
                        </div>
                    </button>

                    <ul class="fws-max-h-0 fws-opacity-0 fws-px-5 fws-col-start-1 fws-col-end-7 fws-row-start-2 fws-row-end-3 fws-overflow-auto fws-flex-col fws-justify-center fws-items-center fws-transform fws-transition-all fws-duration-300" data-fws-submenu="<?php echo $submenuItem->ID ?>" style="background: <?php echo $menu->getCurrenPageBgColor() ?>;">

                        <?php

                        foreach ($subSubMenuItems as $subSubMenuItem) {
                        ?>
                            <li class="fws-my-2 hover:fws-underline fws-underline-offset-4 fws-text-white fws-text-sm lg:fws-text-base">
                                <a class="fws-text-left fws-font-semibold" href="<?php echo esc_url($subSubMenuItem->url); ?>" title="<?php echo esc_attr($subSubMenuItem->title); ?>">
                                    <?php echo esc_html($subSubMenuItem->title); ?>
                                </a>

                            </li>

                        <?php
                        }
                        ?>
                    </ul>
                <?php

                }
                ?>
            </div>
        <?php
        }
        ?>
    </nav>
</div>