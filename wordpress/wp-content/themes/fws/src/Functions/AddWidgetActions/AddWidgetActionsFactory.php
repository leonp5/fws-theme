<?php

namespace Leonp5\fws\Functions\AddWidgetActions;

use Psr\Container\ContainerInterface;

use Leonp5\fws\App\Interfaces\FactoryInterface;

class AddWidgetActionsFactory implements FactoryInterface
{
    public static function create(ContainerInterface $container): AddWidgetActionsInterface
    {
        return new AddWidgetActions();
    }
}
