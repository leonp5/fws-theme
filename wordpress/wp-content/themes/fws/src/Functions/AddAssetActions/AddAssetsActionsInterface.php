<?php

namespace Leonp5\fws\Functions\AddAssetActions;

use Leonp5\fws\Functions\AddActionsInterface;

interface AddAssetsActionsInterface extends AddActionsInterface
{
    public function extendThemeSupport(): void;

    public function registerAssets(): void;

    public function registerAdminStyles(): void;
}
