import * as classes from './classes'

export default class Initializer {
    elementsWithAttributes: Element[]

    init(): void {
        setTimeout(() => {
            this.elementsWithAttributes = Array.from(
                document.querySelectorAll(
                    `[data-${globalThis.appShortCut}-class]`
                )
            )

            if (this.elementsWithAttributes.length > 0) {
                try {
                    this.elementsWithAttributes.forEach((htmlElement) => {
                        const className = htmlElement.getAttribute(
                            `data-${globalThis.appShortCut}-class`
                        )

                        if (className !== null) {
                            const classToExecute = this.factory(className)
                            classToExecute.init(htmlElement)
                        }
                    })
                } catch (e) {
                    console.error(e)
                }
            }
        }, 100)
    }

    factory(className: string) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return new (<any>classes)[className]()
    }
}
