<?php

namespace Leonp5\fws\Theme\partials\PostPagination;

interface PostPaginationInterface
{
    public function getPagination(): ?string;
}
