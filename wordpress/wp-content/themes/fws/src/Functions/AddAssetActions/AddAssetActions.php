<?php

declare(strict_types=1);

namespace Leonp5\fws\Functions\AddAssetActions;

final class AddAssetActions implements AddAssetsActionsInterface
{
    private string $env;
    private string $projectName;
    private string $devAppDomain;

    public function __construct(
        string $env,
        string $projectName
    ) {
        $this->env = $env;
        $this->projectName = $projectName;
        $this->devAppDomain = 'https://' . (string) getenv('APP_DOMAIN') . '/' . $this->projectName . '/assets';
    }

    public function addActions(): void
    {
        add_action('wp_enqueue_scripts', [$this, 'registerAssets']);
        add_action('admin_enqueue_scripts', [$this, 'registerAdminStyles']);
        $this->extendThemeSupport();
    }


    public function extendThemeSupport(): void
    {
        // adds dynamic title tag as long as in the head the wp_head() runs
        add_theme_support('title-tag');
        // adds custom logo upload option
        add_theme_support('custom-logo');
    }

    public function registerAssets(): void
    {
        // load prod assets
        if ($this->env !== 'dev') {
            wp_enqueue_style($this->projectName . '.app', get_template_directory_uri() . '/dist/' . $this->projectName . '.app.css', [], '1.0', 'all');
            wp_enqueue_script($this->projectName . '.app', get_template_directory_uri() . '/dist/' . $this->projectName . '.app.js', [], '1.0', true);
            return;
        }

        // load dev assets (works only if the webpack devServer runs)
        if ($this->env === 'dev') {
            wp_enqueue_style($this->projectName . '.app.dev', $this->devAppDomain . '/' . $this->projectName . '.app.css');
            wp_enqueue_script($this->projectName . '.app.dev', $this->devAppDomain . '/' . $this->projectName . '.app.js', [], '1.0', true);
        }
    }

    public function registerAdminStyles(): void
    {
        // load prod assets
        if ($this->env !== 'dev') {
            wp_enqueue_style($this->projectName . '.admin', get_template_directory_uri() . '/dist/' . $this->projectName . '.admin.css', [], '1.0', 'all');
            return;
        }

        // load dev assets (works only if the webpack devServer runs)
        if ($this->env === 'dev') {
            wp_enqueue_style($this->projectName . '.admin.dev', $this->devAppDomain . '/' . $this->projectName . '.admin.css');
            // only needed for webpack live reload
            wp_enqueue_script($this->projectName . '.admin.dev', $this->devAppDomain . '/' . $this->projectName . '.admin.js', [], '1.0', true);
        }
    }
}
