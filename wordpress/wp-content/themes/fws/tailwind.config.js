/** @type {import('tailwindcss').Config} */
module.exports = {
    prefix: 'fws-',
    content: ['./assets/js/**/*.ts', './*.php', './**/*.php'],
    theme: {
        extend: {
            colors: {
                red: {
                    900: '#800000',
                },
            },
        },
    },

    plugins: [],
}
