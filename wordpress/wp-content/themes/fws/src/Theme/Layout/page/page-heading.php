<?php

declare(strict_types=1);

/**
 * Template page heading
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Theme\partials\Menu\Menu;

$container = (new Container())->getInstance();
$menu = $container->get(Menu::class);

?>

<h1 class="fws-text-gray-700 fws-mb-5 fws-font-semibold" style="color: <?php echo $menu->getCurrenPageBgColor() ?>;"><?php echo esc_html($args['title']); ?></h1>
