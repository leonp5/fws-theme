<?php

declare(strict_types=1);

/**
 * Template functions
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Functions\AddMenuActions\AddMenuActions;
use Leonp5\fws\Functions\AddAssetActions\AddAssetActions;
use Leonp5\fws\Functions\AddWidgetActions\AddWidgetActions;

include_once(get_template_directory() . '/vendor/autoload.php');
$container = (new Container())->getInstance();

$themeActions = $container->get(AddAssetActions::class);
$themeActions->addActions();
$addMenuActions = $container->get(AddMenuActions::class);
$addMenuActions->addActions();
$addWidgetActions = $container->get(AddWidgetActions::class);
$addWidgetActions->addActions();
