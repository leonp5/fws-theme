<?php

declare(strict_types=1);

namespace Leonp5\fws\Theme\partials\Menu;

use Leonp5\fws\Theme\partials\Menu\Hierarchy\MenuHierarchyInterface;
use stdClass;

use function array_key_exists;

final class Menu implements MenuInterface
{

    private ?array $menus = null;
    private ?array $locations = null;
    private ?int $queriedObjectId = null;
    private ?string $currentPageBgColor = null;
    private string $defaultColor;

    public function __construct(
        string $defaultBgColor
    ) {
        $this->defaultColor = $defaultBgColor;
    }

    public function getMenuItemsByKey(string $key): array
    {
        // avoid double DB requests

        if ($this->menus !== null && array_key_exists($key, $this->menus) === true) {
            return $this->menus[$key];
        }

        $locations = $this->findNavMenuLocations();

        $menu_id = !empty($locations[$key]) ? $locations[$key] : '';

        $menuId = !empty($menu_id) ? $menu_id : '';

        $menuItems = wp_get_nav_menu_items($menuId);

        foreach ($menuItems as $menuItem) {
            $menuItem->bg_color = get_post_meta($menuItem->ID, '_bg_color', true);
        }

        $this->menus[$key] = $menuItems;

        return $this->menus[$key];
    }

    public function getMenuItemsByMenuId(int $menuId): array
    {
        $locations = $this->findNavMenuLocations();

        foreach ($locations as $key => $id) {
            if ($id === $menuId) {
                return $this->getMenuItemsByKey($key);
            }
        }

        return [];
    }

    public function getFirstOrderMenuItemsByKey(string $key): array
    {
        $firstOrderMenuItems = [];

        $menuItems = $this->getMenuItemsByKey($key);

        foreach ($menuItems as $menuItem) {
            if ($menuItem->menu_item_parent === '0') {
                $firstOrderMenuItems[] = $menuItem;
            }
        }

        return $firstOrderMenuItems;
    }

    public function findMenuLocationKeyByMenuItemId(int $menuItemId): ?string
    {
        $locations = $this->findNavMenuLocations();

        foreach ($locations as $key => $id) {
            $menuItems = $this->getMenuItemsByKey($key);

            foreach ($menuItems as $menuItem) {
                if ($menuItem->ID === $menuItemId) {
                    return $key;
                }
            }
        }

        return null;
    }

    public function findSubemenuItems(int $menuItemId, ?string $menuKey): array
    {
        $submenuItems = [];

        if ($menuKey !== null) {
            $menuItems = $this->getMenuItemsByKey($menuKey);

            foreach ($menuItems as $menuItem) {
                if ((int) $menuItem->menu_item_parent === $menuItemId) {
                    $submenuItems[] = $menuItem;
                }
            }
        } else {
            $locations = $this->findNavMenuLocations();

            foreach ($locations as $key => $id) {
                $menuItems = $this->getMenuItemsByKey($key);

                foreach ($menuItems as $menuItem) {
                    if ((int) $menuItem->menu_item_parent === $menuItemId) {
                        $submenuItems[] = $menuItem;
                    }
                }
            }
        }

        return $submenuItems;
    }

    public function hasSubmenu(int $menuItemId, string $menuKey): bool
    {
        $menuItems = $this->getMenuItemsByKey($menuKey);

        foreach ($menuItems as $menuItem) {
            if ((int) $menuItem->menu_item_parent === $menuItemId) {
                return true;
            }
        }

        return false;
    }

    public function hasParent(int $menuItemId): bool
    {
        $menuItem = $this->findMenuItemByMenuItemId($menuItemId);

        return (int) $menuItem->menu_item_parent !== 0;
    }

    public function findMenuLocationKeyByMenuId(int $menuId): ?string
    {
        $locations = $this->findNavMenuLocations();

        foreach ($locations as $key => $id) {
            if ($id === $menuId) {
                return $key;
            }
        }

        return null;
    }

    public function getAllLocations(): array
    {
        if ($this->locations !== null) {
            return $this->locations;
        }

        return $this->findNavMenuLocations();
    }

    private function findNavMenuLocations(): array
    {
        if ($this->locations !== null) {
            return $this->locations;
        }

        $this->locations = get_nav_menu_locations();

        return $this->locations;
    }

    public function findMenuItemByObjectId(int $objectId): ?object
    {
        $landingPageId = (int) get_option('page_on_front');

        if ($landingPageId !== 0 && $landingPageId === $objectId) {
            return $this->getLandingPagePost($landingPageId);
        }

        $locations = $this->findNavMenuLocations();

        foreach ($locations as $key => $id) {
            $menuItems = $this->getMenuItemsByKey($key);

            foreach ($menuItems as $menuItem) {
                if ((int) $menuItem->object_id === $objectId) {
                    return $menuItem;
                }
            }
        }

        $eventCalenderPost = get_post($objectId);

        return null;
    }

    public function findMenuItemByMenuItemId(int $menuItemId): ?object
    {
        $landingPageId = (int) get_option('page_on_front');

        if ($landingPageId !== 0 && $landingPageId === $menuItemId) {
            return $this->getLandingPagePost($landingPageId);
        }

        $locations = $this->findNavMenuLocations();

        foreach ($locations as $key => $id) {
            $menuItems = $this->getMenuItemsByKey($key);

            foreach ($menuItems as $menuItem) {
                if ($menuItem->ID === $menuItemId) {
                    return $menuItem;
                }
            }
        }

        return null;
    }

    private function getLandingPagePost(int $id = null): object
    {
        // set default bg color
        $bgColor = $this->defaultColor;

        if ($id === null) {
            $id = (int) get_option('page_on_front');
        }

        $post = get_post($id);

        $locations = $this->findNavMenuLocations();
        $breakLoop = false;

        // look if the landingpage is part of a menu
        foreach ($locations as $key => $locationId) {
            if ($breakLoop === true) {
                break;
            }

            $menuItems = $this->getMenuItemsByKey($key);

            foreach ($menuItems as $menuItem) {
                if ((int) $menuItem->object_id === $id) {
                    $post = $menuItem;
                    $breakLoop = true;
                    break;
                }
            }
        }

        // if it is the landing page & the landing page isn't inside of a nav menu
        // simulate a nav item object
        if ($breakLoop === false) {
            $post = $this->createTemporaryCustomNavMenuItem(
                $post->post_title,
                get_site_url() . '/',
                $post->menu_order
            );

            $post->bg_color = $bgColor;
        }

        return  $post;
    }

    public function getCurrenPageBgColor(): string
    {
        if ($this->currentPageBgColor !== null) {
            return $this->currentPageBgColor;
        }

        $queriedObjectId = $this->getQueriedObjectId();
        $currentMenuItem = $this->findMenuItemByObjectId($queriedObjectId);

        // if it isn't a page in a navigation and it is a post
        if ($currentMenuItem === null && is_singular('post') === true) {
            $postArchivePageId = (int) get_option('page_for_posts');

            if ($postArchivePageId === '' || $postArchivePageId === null) {
                $this->currentPageBgColor = $this->defaultColor;
                return $this->currentPageBgColor;
            }

            $this->currentPageBgColor = $this->findMenuItemByObjectId($postArchivePageId)->bg_color ?? $this->defaultColor;

            return $this->currentPageBgColor;
        }

        if ($currentMenuItem === null) {
            $this->currentPageBgColor = $this->defaultColor;
            return $this->currentPageBgColor;
        }

        $this->currentPageBgColor = $currentMenuItem->bg_color;

        if ($this->hasParent($currentMenuItem->ID) === true) {
            $parentItem = $this->findMenuItemByMenuItemId((int) $currentMenuItem->menu_item_parent);

            if ($this->hasParent($parentItem->ID) === true) {
                // page is third hierarchy page
                $parentParentItem = $this->findMenuItemByMenuItemId((int) $parentItem->menu_item_parent);
                $this->currentPageBgColor = $parentParentItem->bg_color;
                return $this->currentPageBgColor;
            }

            // page is second hierarchy page
            $this->currentPageBgColor = $parentItem->bg_color;
        }

        return $this->currentPageBgColor;
    }

    public function determineMenuItemHierarchy(int $menuItemId): int
    {
        $menuItem = $this->findMenuItemByMenuItemId($menuItemId);

        if ($this->hasParent($menuItem->ID) === true) {
            $parentItem = $this->findMenuItemByMenuItemId((int) $menuItem->menu_item_parent);

            if ($this->hasParent($parentItem->ID) === true) {
                return MenuHierarchyInterface::MENU_HIERARCHY_THREE;
            }

            return MenuHierarchyInterface::MENU_HIERARCHY_TWO;
        }

        return MenuHierarchyInterface::MENU_HIERARCHY_ONE;
    }

    public function getQueriedObjectId(): int
    {
        if ($this->queriedObjectId !== null) {
            return $this->queriedObjectId;
        }

        $this->queriedObjectId = get_queried_object_id();

        $post = get_post($this->queriedObjectId);

        // Workaround: the theme is intended to be used with the Event Calendar plugin.
        // the overview page from this plugin will return 0 as object id
        if ($this->queriedObjectId === 0 || $post->post_type === 'tribe_events') {
            $eventCalendarReferencePostId = $this->findEventCalendarReferenceMenuPost();
            $this->queriedObjectId = $eventCalendarReferencePostId === null ? $this->queriedObjectId : $eventCalendarReferencePostId;
        }

        return $this->queriedObjectId;
    }

    private function findEventCalendarReferenceMenuPost(): int | null
    {
        global $wpdb;

        $sql = 'SELECT * FROM ' . $wpdb->postmeta . ' WHERE `meta_key` = %s AND `meta_value` = %s';

        $sql = $wpdb->prepare($sql, ['event_calendar_reference_page', '1']);

        $result = $wpdb->get_results($sql);

        if (count($result) === 0) {
            return null;
        }

        $postMeta = get_post_meta($result[0]->post_id);

        return (int) $postMeta['_menu_item_object_id'][0];
    }

    /**
     * @param $title      - menu item title
     * @param $url        - menu item url
     * @param $order      - where the item should appear in the menu
     * @param int $parent - the item's parent item
     * @return \stdClass
     */
    private function createTemporaryCustomNavMenuItem($title, $url, $order, $parent = 0)
    {
        $item = new stdClass();
        $item->ID = 1000000 + $order + $parent;
        $item->db_id = $item->ID;
        $item->title = $title;
        $item->url = $url;
        $item->menu_order = $order;
        $item->menu_item_parent = $parent;
        $item->type = '';
        $item->object = '';
        $item->object_id = '';
        $item->classes = array();
        $item->target = '';
        $item->attr_title = '';
        $item->description = '';
        $item->xfn = '';
        $item->status = '';
        return $item;
    }
}
