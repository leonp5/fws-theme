<?php

namespace Leonp5\fws\Functions\AddWidgetActions;

class AddWidgetActions implements AddWidgetActionsInterface
{
    public function addActions(): void
    {
        add_action('widgets_init', [$this, 'addWidgetAreas']);
    }

    public function addWidgetAreas(): void
    {
        if (function_exists('register_sidebar') === true) {

            register_sidebar(
                array(
                    'name' => 'Header Widget',
                    'id' => 'fws_header_widget',
                    'description' => 'Widget/Plugin im Header platzieren. z.B. einen Slider',
                    'class' => '',
                    'before_widget' => '',
                    'after_widget' => '',
                    'before_title' => '',
                    'after_title' => '',
                    'before_sidebar' => '',
                    'after_sidebar' => '',
                    'show_in_rest' => true
                )
            );
        }
    }
}
