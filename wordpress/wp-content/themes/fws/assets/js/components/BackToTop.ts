import InitialClassInterface from '../global/interfaces/InitialClassInterface'

export default class BackToTop implements InitialClassInterface {
    init(btnElement: HTMLElement): void {
        btnElement.addEventListener('click', () => {
            this.scrollToTop()
        })

        window.addEventListener('scroll', () => {
            if (
                document.body.scrollTop > 270 ||
                document.documentElement.scrollTop > 270
            ) {
                btnElement.classList.remove('fws-hidden')
            } else {
                btnElement.classList.add('fws-hidden')
            }
        })
    }

    scrollToTop(): void {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth',
        })
    }
}
