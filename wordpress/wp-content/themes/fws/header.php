<?php

declare(strict_types=1);

/**
 * Header template
 *
 * @package fws_melle_theme
 */

use Leonp5\fws\App\Container;
use Leonp5\fws\Theme\partials\Menu\Menu;

$container = (new Container())->getInstance();
$menu = $container->get(Menu::class);

?>

<div style="background: <?php echo $menu->getCurrenPageBgColor() ?>;" class="fws-fixed fws-col-start-1 fws-col-end-10 fws-top-0 fws-z-10 fws-flex fws-w-full fws-justify-between fws-items-center fws-pr-5 md:fws-hidden fws-shadow" data-fws-class="ToggleMobileMenu">
    <a class="fws-ml-5" href="<?php echo get_home_url() ?>">
        <img class="fws-max-h-[50px]" src="<?php echo $container->get('config')['assetUri'] . '/images/fwsm_ar_logo_small.jpg' ?>" alt="Freie Schule Melle Logo">
    </a>

    <?php
    get_template_part('src/Theme/Layout/mobileMenu');

    ?>
</div>
<div class="fws-hidden md:fws-flex fws-justify-center fws-col-start-1 fws-col-end-3 fws-row-start-2 md:fws-row-start-1 fws-row-end-3 md:fws-row-end-4 fws-rounded-tl md:fws-h-[220px] lg:fws-h-[260px] xl:fws-h-[300px]">
    <a class="fws-flex fws-justify-center" href="<?php echo get_home_url() ?>">
        <?php
        if (
            function_exists('the_custom_logo') &&
            get_theme_mod('custom_logo') !== '' &&
            get_theme_mod('custom_logo') !== false
        ) {
            $customLogoId = get_theme_mod('custom_logo');
            $logo = wp_get_attachment_image_src($customLogoId);
            $logoSrc = $logo[0];
        } else {
            $logoSrc = $container->get('config')['assetUri'] . '/images/fwsm_ar_logo.jpg';
        }
        ?>
        <img class="fws-h-full" src="<?php echo $logoSrc ?>" alt="Freie Schule Melle Logo">
        <?php
        ?>
    </a>
</div>

<div class="fws-flex fws-flex-col fws-col-start-1 md:fws-col-start-3 fws-col-end-10 fws-row-start-2 md:fws-row-start-1 md:fws-row-end-4 fws-row-end-3">
    <div class="fws-pt-[50px] md:fws-pt-0 fws-block fws-w-full md:fws-max-h-[250px] fws-overflow-hidden md:fws-rounded-tr">
        <?php
        if (function_exists('dynamic_sidebar')) {
            dynamic_sidebar('fws_header_widget');
        }
        ?>
    </div>

    <div class="fws-hidden md:fws-block">
        <nav class="fws-flex fws-relative fws-h-[70px] lg:fws-h-[80px]">
            <?php
            $primaryMenuItems = $menu->getFirstOrderMenuItemsByKey('primary');
            foreach ($primaryMenuItems as $menuItem) {
            ?>

                <a class="fws-px-2 fws-py-4 hover:fws-opacity-80 fws-text-white fws-font-semibold fws-w-full fws-text-center fws-text-sm lg:fws-text-base" style="background: <?php echo esc_html($menuItem->bg_color) ?>;" href="<?php echo esc_url($menuItem->url); ?>" title="<?php echo esc_attr($menuItem->title); ?>">
                    <?php echo esc_html($menuItem->title); ?>
                </a>

            <?php
            }

            ?>
        </nav>
        <div class="fws-z-10 fws-relative fws-mt-[-25px] lg:fws-mt-[-32px]">
            <div class="fws-h-[60px] fws-overflow-hidden "><svg viewBox="0 40 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
                    <path d="M-3.10,82.40 C153.78,44.90 226.01,136.67 500.27,42.92 L501.40,152.47 L0.00,150.00 Z" style="stroke: none; fill: #ffffff;"></path>
                </svg>
            </div>
        </div>
    </div>

</div>