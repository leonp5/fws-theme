import InitialClassInterface from '../global/interfaces/InitialClassInterface'

export default class ToggleMobileMenu implements InitialClassInterface {
    mobileHeaderElement: HTMLDivElement
    buttonEl: HTMLButtonElement
    hamburgerTransAnimationWrapper: HTMLDivElement
    hamburgerBtnFirstRow: HTMLDivElement
    hamburgerBtnSecondRow: HTMLDivElement
    hamburgerBtnThirdRow: HTMLDivElement
    mobileMenuEl: HTMLDivElement
    mobileMenuIsOpen: boolean

    constructor() {
        this.mobileMenuIsOpen = false
    }

    init(mobileHeaderElement: HTMLDivElement): void {
        this.mobileHeaderElement = mobileHeaderElement

        this.buttonEl = mobileHeaderElement.querySelector(
            'button'
        ) as HTMLButtonElement

        this.mobileMenuEl = document.getElementById(
            `${globalThis.appShortCut}-mobile-menu`
        ) as HTMLDivElement

        this.hamburgerTransAnimationWrapper = document.getElementById(
            `${globalThis.appShortCut}-hamburger-trans-animation-wrapper`
        ) as HTMLDivElement

        if (this.buttonEl !== undefined && this.mobileMenuEl !== undefined) {
            this.hamburgerBtnFirstRow = this.buttonEl.querySelectorAll('div')[0]
            this.hamburgerBtnSecondRow =
                this.buttonEl.querySelectorAll('div')[1]
            this.hamburgerBtnThirdRow = this.buttonEl.querySelectorAll('div')[2]

            this.buttonEl.addEventListener('click', () => {
                this.toggleMenu()
            })
        }
    }

    toggleMenu(): void {
        this.handleButtonClassListToggles()

        if (this.mobileMenuIsOpen === false) {
            this.setMobileMenuShowStyles()
        } else {
            this.removeMobileMenuShowStyles()
        }

        this.mobileMenuIsOpen = !this.mobileMenuIsOpen
    }

    handleButtonClassListToggles(): void {
        const firstAnimationWrapperChildEl =
            this.hamburgerTransAnimationWrapper.querySelectorAll('div')[0]

        const secondAnimationWrapperChildEl =
            this.hamburgerTransAnimationWrapper.querySelectorAll('div')[1]

        this.hamburgerBtnFirstRow.classList.toggle('fws-translate-x-10')
        this.hamburgerBtnSecondRow.classList.toggle('fws-translate-x-10')
        this.hamburgerBtnThirdRow.classList.toggle('fws-translate-x-10')

        this.hamburgerTransAnimationWrapper.classList.toggle('fws-w-0')
        this.hamburgerTransAnimationWrapper.classList.toggle('fws-w-12')
        this.hamburgerTransAnimationWrapper.classList.toggle(
            'fws-translate-x-0'
        )
        this.hamburgerTransAnimationWrapper.classList.toggle(
            '-fws-translate-x-10'
        )

        firstAnimationWrapperChildEl.classList.toggle('fws-rotate-0')
        firstAnimationWrapperChildEl.classList.toggle('fws-rotate-45')
        secondAnimationWrapperChildEl.classList.toggle('-fws-rotate-0')
        secondAnimationWrapperChildEl.classList.toggle('-fws-rotate-45')

        // Hide body scrollbar if exists
        document.querySelector('body')?.classList.toggle('fws-overflow-hidden')
    }

    removeMobileMenuShowStyles(): void {
        this.mobileMenuEl.classList.add('fws-opacity-0')
        this.mobileMenuEl.classList.remove('fws-opacity-100')

        setTimeout(() => {
            this.mobileMenuEl.classList.add('fws-hidden')
            this.mobileMenuEl.classList.remove('fws-flex')
            this.mobileMenuEl.removeAttribute('style')
        }, 100)
    }

    setMobileMenuShowStyles(): void {
        const height =
            window.innerHeight - this.mobileHeaderElement.offsetHeight
        this.mobileMenuEl.style.height = height + 'px'
        this.mobileMenuEl.style.top =
            this.mobileHeaderElement.offsetHeight + 'px'

        this.mobileMenuEl.classList.remove('fws-hidden')
        this.mobileMenuEl.classList.add('fws-flex')
        setTimeout(() => {
            this.mobileMenuEl.classList.remove('fws-opacity-0')
            setTimeout(() => {
                this.mobileMenuEl.classList.add('fws-opacity-100')
            }, 50)
        }, 50)
    }
}
