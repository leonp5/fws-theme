<?php

declare(strict_types=1);

namespace Leonp5\fws\App\Interfaces;

interface DependencyProviderInterface
{

    /**
     * @return Array<string, string>
     */
    public function getAliases(): array;
}
