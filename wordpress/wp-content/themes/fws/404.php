<?php

declare(strict_types=1);

/**
 * Template 404 page
 *
 * @package fws_melle_theme
 */

// use Leonp5\fws\App\Container;

// $container = (new Container())->getInstance();

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_template_part('src/Theme/Layout/head'); ?>

<body class="fws-bg-white">
    <div class="fws-container fws-mx-auto fws-flex fws-min-h-[95vh] fws-justify-center">
        <div class="fws-flex fws-flex-col fws-max-w-[1200px] fws-w-full fws-pt-4 fws-shadow-lg fws-rounded">

            <?php get_header() ?>

            <div class="fws-grow fws-flex fws-flex-col fws-justify-center fws-items-center">
                <div class="fws-flex">
                    <h1 class="fws-text-red-700 fws-text-7xl fws-font-bold fws-tracking-wider">4</h1>
                    <h1 class="fws-text-7xl fws-font-bold fws-tracking-wider">0</h1>
                    <h1 class="fws-text-red-700 fws-text-7xl fws-font-bold fws-tracking-wider">4</h1>
                </div>
                <p class="fws-my-5 fws-text-black">Die Seite wurde nicht gefunden.</p>
                <a class="fws-text-blue-800 fws-font-semibold" href="<?php echo get_home_url() ?>">Zur Startseite</a>
            </div>
            <?php get_footer() ?>
        </div>
    </div>
</body>


</html>
