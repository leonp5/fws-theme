// If the the following error occurs, it may help to install node types:
// "Element implicitly has an 'any' type because type 'typeof globalThis' has no index signature"
// Install node types: npm install -D @types/node

export interface global {}
export interface globalThis {}

declare global {
    var appShortCut: string
}
