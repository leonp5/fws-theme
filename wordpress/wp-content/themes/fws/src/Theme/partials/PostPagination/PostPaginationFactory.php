<?php

declare(strict_types=1);

namespace Leonp5\fws\Theme\partials\PostPagination;

use Leonp5\fws\Theme\partials\Menu\Menu;
use Psr\Container\ContainerInterface;

class PostPaginationFactory
{
    public static function create(ContainerInterface $container): PostPaginationInterface
    {
        return new PostPagination(
            $container->get(Menu::class)->getCurrenPageBgColor()
        );
    }
}
